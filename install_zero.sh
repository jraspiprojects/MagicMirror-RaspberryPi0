#!/usr/bin/env bash



echo '$$\      $$\                     $$\           $$\      $$\ $$\                                          $$$$$$\'
echo '$$$\    $$$ |                    \__|          $$$\    $$$ |\__|                                        $$  __$$\'
echo '$$$$\  $$$$ | $$$$$$\   $$$$$$\  $$\  $$$$$$$\ $$$$\  $$$$ |$$\  $$$$$$\   $$$$$$\   $$$$$$\   $$$$$$\  \__/  $$ |'
echo '$$\$$\$$ $$ | \____$$\ $$  __$$\ $$ |$$  _____|$$\$$\$$ $$ |$$ |$$  __$$\ $$  __$$\ $$  __$$\ $$  __$$\  $$$$$$  |'
echo '$$ \$$$  $$ | $$$$$$$ |$$ /  $$ |$$ |$$ /      $$ \$$$  $$ |$$ |$$ |  \__|$$ |  \__|$$ /  $$ |$$ |  \__|$$  ____/'
echo '$$ |\$  /$$ |$$  __$$ |$$ |  $$ |$$ |$$ |      $$ |\$  /$$ |$$ |$$ |      $$ |      $$ |  $$ |$$ |      $$ |'
echo '$$ | \_/ $$ |\$$$$$$$ |\$$$$$$$ |$$ |\$$$$$$$\ $$ | \_/ $$ |$$ |$$ |      $$ |      \$$$$$$  |$$ |      $$$$$$$$\'
echo '\__|     \__| \_______| \____$$ |\__| \_______|\__|     \__|\__|\__|      \__|       \______/ \__|      \________|'
echo '                       $$\   $$ |'
echo '                       \$$$$$$  |'
echo '                        \______/'
echo -e "\e[0m
echo 'Installer for raspberry pi one and zero'
echo 'Starting....'
echo 'I assume you have nodejs and npm installed'
echo 'Installing prerequites...'
sudo apt-get install -y git wget
echo 'Installed'
echo 'Moving to $HOME'
cd ~/
echo 'Cloning'
git clone https://github.com/MichMich/MagicMirror
echo 'Installing'
cd MagicMirror
sudo npm install
echo 'Installed'
echo 'Installing second set of prerequestes'
sudo apt-get install -y midori xinit xorg matchbox unclutter curl
cd ../
wget https://github.com/jraspiprojects/MagicMirror-RaspberryPi0/raw/master/mmstart.sh
wget https://github.com/jraspiprojects/MagicMirror-RaspberryPi0/raw/master/midori_start.sh
chmod +x *.sh
echo 'Done'
